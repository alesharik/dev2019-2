const express = require('express');
const proxy = require('http-proxy-middleware');
const path = require("path");

let app = express();

app.use("/", express.static("dist"));
app.use(proxy('/api', { target: 'http://localhost:8080/' }));

app.get('*', (req,res) =>{
    res.sendFile(path.join(__dirname+'/dist/index.html'));
});

app.listen(3000,(req,res)=>{
    console.log('server running on port 3000')
});