import * as React from "react";
import * as ReactDOM from "react-dom";
import './style.less';
import 'semantic-ui-css/semantic.min.css';
import {Api} from "./api/Api";
import {WiredAppMenu} from "./component/WiredAppMenu";
import { BrowserRouter as Router, Route } from "react-router-dom";
import {Login} from "./component/Login";
import {Register} from "./component/Register";
import {Main} from "./component/Main";
import {Algo} from "./component/Algo";
import {Crypt} from "./component/Crypt";
import {Restore} from "./component/Restore";

export const api = new Api();

class App extends React.Component {
  render() {
    return (
      <Router>
        <Route path={"/login"} exact component={Login}/>
        <Route path={"/register"} exact component={Register}/>
        <Route path={"/restore"} exact component={Restore}/>
        <Route path={"/algo"} exact component={Algo}/>
        <Route path={"/crypt"} exact component={Crypt}/>
        <Route exact path={"/"} component={Main}/>
      </Router>
    );
  }
}
ReactDOM.render(
  <App />,
  document.getElementById("root")
);
