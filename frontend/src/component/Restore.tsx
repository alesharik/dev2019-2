import * as React from "react";
import {Container, Form, Grid, Message} from "semantic-ui-react";
import {api} from "../app";
import {Api} from "../api/Api";

class RestoreState {
    public login: string;
    public ok: boolean;
    public loading: boolean;
    public error: string;

    constructor(login: string, ok: boolean, loading: boolean, error: string) {
        this.login = login;
        this.ok = ok;
        this.loading = loading;
        this.error = error;
    }
}

export class Restore extends React.Component<any, RestoreState> {

    constructor(props: any, context: any) {
        super(props, context);
        this.state = new RestoreState("",true, false, "");
        this.updateLogin = this.updateLogin.bind(this);
        this.submit = this.submit.bind(this);
    }

    updateLogin(e, {value}) {
        this.setState(new RestoreState(value, this.state.ok, false, ""));
    }

    submit() {
        // noinspection JSIgnoredPromiseFromCall
        Api.restore(this.state.login)
            .then(() => {
                this.props.history.push("/")
            }).catch(reason => {
                this.setState(new RestoreState(this.state.login, false, true, reason.toString()));
            });
        this.setState(new RestoreState(this.state.login, this.state.ok, true, ""));
    }

    render() {
        return <Container><Grid className={"segment centered"}>
            <Form error={!this.state.ok}>
                <Form.Field>
                    <Form.Input placeholder={"Login"} onChange={this.updateLogin} value={this.state.login}/>
                </Form.Field>
                {this.state.ok ? <div/> : <Message error header={"Login failed"} content={this.state.error}/>}
                <Form.Button content={"Restore"} onClick={this.submit}/>
            </Form>
        </Grid></Container>;
    }
}