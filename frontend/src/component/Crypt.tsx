import * as React from 'react';
import {history} from 'history';
import {WiredAppMenu} from "./WiredAppMenu";
import {AlgoEntity, Api, CryptEntity, Entity} from "../api/Api";
import {api} from "../app";
import {Dimmer, Loader} from "semantic-ui-react";
import ReactTable from "react-table";
import "react-table/react-table.css";
import matchSorter from 'match-sorter'

export class CryptProps {
    public history: history;

    constructor(history: history) {
        this.history = history;
    }
}

class CryptState {
    public entities: CryptEntity[];
    public loading: boolean;

    constructor(entities: CryptEntity[], loading: boolean) {
        this.entities = entities;
        this.loading = loading;
    }
}

export class Crypt extends React.Component<CryptProps, CryptState> {
    constructor(props: CryptProps, context: any) {
        super(props, context);
        this.state = new CryptState([], true);
        let that = this;
        // @ts-ignore
        Api.queryCrypt()
            .then(value => {
                that.setState(new CryptState(value, false));
            })
    }

    render() {
        return (
            <div>
                <Dimmer active={this.state.loading}>
                    <Loader/>
                </Dimmer>
                <WiredAppMenu history={this.props.history}/>
                <ReactTable data={this.state.entities} filterable  defaultFilterMethod={(filter, row) =>
                    String(row[filter.id]) === filter.value} defaultPageSize={19} className="-striped -highlight"
                            columns={[
                                {
                                    columns: [
                                        {
                                            Header: "IN",
                                            id: "in",
                                            accessor: d => d.in,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["in"] }),
                                            filterAll: true
                                        }
                                    ]
                                },
                                {
                                    columns: [
                                        {
                                            Header: "CODED",
                                            id: "coded",
                                            accessor: d => d.coded,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["coded"] }),
                                            filterAll: true
                                        }
                                    ]
                                },
                                {
                                    columns: [
                                        {
                                            Header: "DECODED",
                                            id: "decoded",
                                            accessor: d => d.decoded,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["decoded"] }),
                                            filterAll: true
                                        }
                                    ]
                                }]}
                />
            </div>
        )
    }
}