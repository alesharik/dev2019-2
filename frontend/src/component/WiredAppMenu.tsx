import * as React from 'react';
import {Api} from "../api/Api";
import {AppMenu} from "./AppMenu";
import {history} from "history";
import {api} from "../app";

export class WiredAppMenuProps {
    public history: history;

    constructor(history:history) {
        this.history = history;
    }
}

class WiredAppMenuState {
    tags: string[];
    loading: boolean;

    constructor(tags: string[], loading: boolean) {
        this.tags = tags;
        this.loading = loading;
    }
}

export class WiredAppMenu extends React.Component<WiredAppMenuProps, WiredAppMenuState> {
    constructor(props: WiredAppMenuProps, context: any) {
        super(props, context);
        this.state = new WiredAppMenuState([], true);
    }

    render() {
        return (<AppMenu isLogged={api.isLoggedIn} login={api.isLoggedIn ? api.name : undefined} loading={this.state.loading} history={this.props.history}/>);
    }
}