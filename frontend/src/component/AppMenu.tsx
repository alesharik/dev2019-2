import * as React from 'react';
import {Button, Dropdown, Menu} from "semantic-ui-react";
import {history} from 'history';
import {api} from "../app";

export class AppMenuProps {
    public isLogged: boolean;
    public login: string;
    public loading: boolean;
    public history: history;


    constructor(isLogged: boolean, login: string, loading: boolean, history: history) {
        this.isLogged = isLogged;
        this.login = login;
        this.loading = loading;
        this.history = history;
    }
}

export class AppMenu extends React.Component<AppMenuProps> {
    constructor(props: AppMenuProps, context: any) {
        super(props, context);
    }

    render() {
        // noinspection SillyAssignmentJS
        return <Menu inverted className={"attached"}>
            <Menu.Item>
                <span>
                    <Button inverted onClick={() => this.props.history.push("/algo")}>Algo</Button>
                    <Button inverted onClick={() => this.props.history.push("/crypt")}>Crypt</Button>
                </span>
            </Menu.Item>
            <Menu.Item position={"right"}>
                {this.props.isLogged ? <Dropdown pointing text={this.props.login}>
                    <Dropdown.Menu>
                        <Dropdown.Item
                            onClick={() => api.logout().then(() => window.location.href = window.location.href)}>
                            Logout
                        </Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown> : <span>
                        <Button primary={true} inverted
                                onClick={() => this.props.history.push("/login")}>Login</Button>
                        <Button inverted onClick={() =>
                            this.props.history.push("/register")}>Register</Button>
                    <Button inverted onClick={() => this.props.history.push("/restore")}>Restore</Button>
                </span>}
            </Menu.Item>
        </Menu>;
    }
}