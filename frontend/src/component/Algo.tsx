import * as React from 'react';
import {history} from 'history';
import {WiredAppMenu} from "./WiredAppMenu";
import {AlgoEntity, Api, Entity} from "../api/Api";
import {api} from "../app";
import {Dimmer, Loader} from "semantic-ui-react";
import ReactTable from "react-table";
import "react-table/react-table.css";
import matchSorter from 'match-sorter'

export class AlgoProps {
    public history: history;

    constructor(history: history) {
        this.history = history;
    }
}

class AlgoState {
    public entities: AlgoEntity[];
    public loading: boolean;

    constructor(entities: AlgoEntity[], loading: boolean) {
        this.entities = entities;
        this.loading = loading;
    }
}

export class Algo extends React.Component<AlgoProps, AlgoState> {
    constructor(props: AlgoProps, context: any) {
        super(props, context);
        this.state = new AlgoState([], true);
        let that = this;
        // @ts-ignore
        Api.queryAlgo()
            .then(value => {
                that.setState(new AlgoState(value, false));
            })
    }

    render() {
        return (
            <div>
                <Dimmer active={this.state.loading}>
                    <Loader/>
                </Dimmer>
                <WiredAppMenu history={this.props.history}/>
                <ReactTable data={this.state.entities} filterable  defaultFilterMethod={(filter, row) =>
                    String(row[filter.id]) === filter.value} defaultPageSize={19} className="-striped -highlight"
                            columns={[
                                {
                                    columns: [
                                        {
                                            Header: "НОМЕР ПРОВЕРКИ В СИСТЕМЕ АС СППиВП",
                                            id: "nomer_proverki_v_sisteme_as_sppivp",
                                            accessor: d => d.nomer_proverki_v_sisteme_as_sppivp,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["nomer_proverki_v_sisteme_as_sppivp"] }),
                                            filterAll: true,
                                            sortable: false
                                        }
                                    ]
                                },
                                {
                                    columns: [
                                        {
                                            Header: "Идентификационный номер налогоплательщика (ИНН)",
                                            id: "identifikatsionnyy_nomer_nalogoplatelshchika_inn",
                                            accessor: d => d.identifikatsionnyy_nomer_nalogoplatelshchika_inn,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["identifikatsionnyy_nomer_nalogoplatelshchika_inn"] }),
                                            filterAll: true,
                                            sortable: false
                                        }
                                    ]
                                },
                                {
                                    columns: [
                                        {
                                            Header: "Основной государственный регистрационный номер (ОГРН)",
                                            id: "osnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn",
                                            accessor: d => d.osnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["osnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn"] }),
                                            filterAll: true,
                                            sortable: false
                                        }
                                    ]
                                }]}
                />
            </div>
        )
    }
}