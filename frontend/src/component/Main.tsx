import * as React from 'react';
import {history} from 'history';
import {WiredAppMenu} from "./WiredAppMenu";
import {Api, Entity} from "../api/Api";
import {api} from "../app";
import {Dimmer, Loader} from "semantic-ui-react";
import ReactTable from "react-table";
import "react-table/react-table.css";
import matchSorter from 'match-sorter'

export class MainProps {
    public history: history;

    constructor(history: history) {
        this.history = history;
    }
}

class MainState {
    public entities: Entity[];
    public loading: boolean;

    constructor(entities: Entity[], loading: boolean) {
        this.entities = entities;
        this.loading = loading;
    }
}

export class Main extends React.Component<MainProps, MainState> {
    constructor(props: MainProps, context: any) {
        super(props, context);
        this.state = new MainState([], true);
        let that = this;
        // @ts-ignore
        Api.query()
            .then(value => {
                that.setState(new MainState(value, false));
            })
    }

    render() {
        return (
            <div>
                <Dimmer active={this.state.loading}>
                    <Loader/>
                </Dimmer>
                <WiredAppMenu history={this.props.history}/>
                <ReactTable data={this.state.entities} filterable  defaultFilterMethod={(filter, row) =>
                    String(row[filter.id]) === filter.value} defaultPageSize={19} className="-striped -highlight"
                            columns={[
                                {
                                    Header: 'Id',
                                    id: "id",
                                    accessor: d => d.id,
                                },
                                {
                                    columns: [
                                        {
                                            Header: "Наименование органа государственного контроля (надзора), муниципального контроля",
                                            id: "naimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_munitsipalnogo_kontrolya",
                                            accessor: d => d.naimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_munitsipalnogo_kontrolya,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["naimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_munitsipalnogo_kontrolya"] }),
                                            filterAll: true
                                        }
                                    ]
                                },
                                {
                                    columns: [
                                        {
                                            Header: "Наименование юридического лица (филиала, представительства, обособленного структурного подразделения) (ЮЛ) (ф.и.о. индивидуального предпринимателя (ИП)), деятельность которого подлежит проверке",
                                            id: "naimenovanie_yuridicheskogo_litsa_filiala_predstavitelstva_obosoblennogo_strukturnogo_podrazdeleniya",
                                            accessor: d => d.naimenovanie_yuridicheskogo_litsa_filiala_predstavitelstva_obosoblennogo_strukturnogo_podrazdeleniya,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["naimenovanie_yuridicheskogo_litsa_filiala_predstavitelstva_obosoblennogo_strukturnogo_podrazdeleniya"] }),
                                            filterAll: true
                                        }
                                    ]
                                },
                                {
                                    columns: [
                                        {
                                            Header: "Адрес места нахождения юридического лица",
                                            id: "adres_mesta_nakhozhdeniya_yul",
                                            accessor: d => d.adres_mesta_nakhozhdeniya_yul,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["adres_mesta_nakhozhdeniya_yul"] }),
                                            filterAll: true
                                        }
                                    ]
                                },
                                {
                                    columns: [
                                        {
                                            Header: "Адрес места фактического осуществления деятельности ЮЛ, ИП",
                                            id: "adres_mesta_fakticheskogo_osushchestvleniya_deyatelnosti_yul_ip",
                                            accessor: d => d.adres_mesta_fakticheskogo_osushchestvleniya_deyatelnosti_yul_ip,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["adres_mesta_fakticheskogo_osushchestvleniya_deyatelnosti_yul_ip"] }),
                                            filterAll: true
                                        }
                                    ]
                                },
                                {
                                    columns: [
                                        {
                                            Header: "Адрес места нахождения объектов",
                                            id: "adres_mesta_nakhozhdeniya_obektov",
                                            accessor: d => d.adres_mesta_nakhozhdeniya_obektov,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["adres_mesta_nakhozhdeniya_obektov"] }),
                                            filterAll: true
                                        }
                                    ]
                                },
                                {
                                    columns: [
                                        {
                                            Header: "Основной государственный регистрационный номер (ОГРН)",
                                            id: "osnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn",
                                            accessor: d => d.osnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["osnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn"] }),
                                            filterAll: true
                                        }
                                    ]
                                },
                                {
                                    columns: [
                                        {
                                            Header: "Идентификационный номер налогоплательщика (ИНН)",
                                            id: "identifikatsionnyy_nomer_nalogoplatelshchika_inn",
                                            accessor: d => d.identifikatsionnyy_nomer_nalogoplatelshchika_inn,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["identifikatsionnyy_nomer_nalogoplatelshchika_inn"] }),
                                            filterAll: true
                                        }
                                    ]
                                }, {
                                    columns: [
                                        {
                                            Header: "Цель проведения проверки",
                                            id: "tsel_provedeniya_proverki",
                                            accessor: d => d.tsel_provedeniya_proverki,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["tsel_provedeniya_proverki"] }),
                                            filterAll: true
                                        }
                                    ]
                                }, {
                                    columns: [
                                        {
                                            Header: "Основание проведения проверки: дата государственной регистрации ЮЛ, ИП",
                                            id: "osnovanie_provedeniya_proverki_data_gosudarstvennoy_registratsii_yul_ip",
                                            accessor: d => d.osnovanie_provedeniya_proverki_data_gosudarstvennoy_registratsii_yul_ip,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["osnovanie_provedeniya_proverki_data_gosudarstvennoy_registratsii_yul_ip"] }),
                                            filterAll: true
                                        }
                                    ]
                                }, {
                                    columns: [
                                        {
                                            Header: "Основание проведения проверки: дата окончания последней проверки",
                                            id: "osnovanie_provedeniya_proverki_data_okonchaniya_posledney_proverki",
                                            accessor: d => d.osnovanie_provedeniya_proverki_data_okonchaniya_posledney_proverki,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["osnovanie_provedeniya_proverki_data_okonchaniya_posledney_proverki"] }),
                                            filterAll: true
                                        }
                                    ]
                                }, {
                                    columns: [
                                        {
                                            Header: "Основание проведения проверки: дата начала осуществления ЮЛ, ИП деятельности в соответствии с представленным уведомлением о ее начале деятельности",
                                            id: "osnovanie_provedeniya_proverki_data_nachala_osushchestvleniya_yul_ip_deyatelnosti_v_sootvetstvii_s_p",
                                            accessor: d => d.osnovanie_provedeniya_proverki_data_nachala_osushchestvleniya_yul_ip_deyatelnosti_v_sootvetstvii_s_p,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["osnovanie_provedeniya_proverki_data_nachala_osushchestvleniya_yul_ip_deyatelnosti_v_sootvetstvii_s_p"] }),
                                            filterAll: true
                                        }
                                    ]
                                }, {
                                    columns: [
                                        {
                                            Header: "Основание проведения проверки: иные основания в соответствии с федеральным законом",
                                            id: "osnovanie_provedeniya_proverki_inye_osnovaniya_v_sootvetstvii_s_federalnym_zakonom",
                                            accessor: d => d.osnovanie_provedeniya_proverki_inye_osnovaniya_v_sootvetstvii_s_federalnym_zakonom,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["osnovanie_provedeniya_proverki_inye_osnovaniya_v_sootvetstvii_s_federalnym_zakonom"] }),
                                            filterAll: true
                                        }
                                    ]
                                }, {
                                    columns: [
                                        {
                                            Header: "Дата начала проведения проверки",
                                            id: "data_nachala_provedeniya_proverki",
                                            accessor: d => d.data_nachala_provedeniya_proverki,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["data_nachala_provedeniya_proverki"] }),
                                            filterAll: true
                                        }
                                    ]
                                }, {
                                    columns: [
                                        {
                                            Header: "Срок проведения плановой проверки, рабочих дней",
                                            id: "srok_provedeniya_planovoy_proverki_rabochikh_dney",
                                            accessor: d => d.srok_provedeniya_planovoy_proverki_rabochikh_dney,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["srok_provedeniya_planovoy_proverki_rabochikh_dney"] }),
                                            filterAll: true
                                        }
                                    ]
                                }, {
                                    columns: [
                                        {
                                            Header: "Срок проведения плановой проверки, рабочих часов (для МСП и МКП)",
                                            id: "srok_provedeniya_planovoy_proverki_rabochikh_chasov_dlya_msp_i_mkp",
                                            accessor: d => d.srok_provedeniya_planovoy_proverki_rabochikh_chasov_dlya_msp_i_mkp,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["srok_provedeniya_planovoy_proverki_rabochikh_chasov_dlya_msp_i_mkp"] }),
                                            filterAll: true
                                        }
                                    ]
                                }, {
                                    columns: [
                                        {
                                            Header: "Форма проведения проверки (документарная, выездная, документарная и выездная)",
                                            id: "forma_provedeniya_proverki_dokumentarnaya_vyezdnaya_dokumentarnaya_i_vyezdnaya",
                                            accessor: d => d.forma_provedeniya_proverki_dokumentarnaya_vyezdnaya_dokumentarnaya_i_vyezdnaya,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["forma_provedeniya_proverki_dokumentarnaya_vyezdnaya_dokumentarnaya_i_vyezdnaya"] }),
                                            filterAll: true
                                        }
                                    ]
                                }, {
                                    columns: [
                                        {
                                            Header: "Наименование органа государственного контроля (надзора), органа муниципального контроля, с которым проверка проводится совместно",
                                            id: "naimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_organa_munitsipalnogo_kontrolya_s_kotorym_pro",
                                            accessor: d => d.naimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_organa_munitsipalnogo_kontrolya_s_kotorym_pro,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["naimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_organa_munitsipalnogo_kontrolya_s_kotorym_pro"] }),
                                            filterAll: true
                                        }
                                    ]
                                }, {
                                    columns: [
                                        {
                                            Header: "Информация о постановлении о назначении административного назначения или решении о приостановлении и (или) аннулировании лицензии: Постановление о назначении административного назначения или решении о приостановлении и (или) аннулировании лицензиии",
                                            id: "informatsiya_o_postanovlenii_o_naznachenii_administrativnogo_naznacheniya_ili_reshenii_o_priostanovl",
                                            accessor: d => d.informatsiya_o_postanovlenii_o_naznachenii_administrativnogo_naznacheniya_ili_reshenii_o_priostanovl,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["informatsiya_o_postanovlenii_o_naznachenii_administrativnogo_naznacheniya_ili_reshenii_o_priostanovl"] }),
                                            filterAll: true
                                        }
                                    ]
                                }, {
                                    columns: [
                                        {
                                            Header: "Информация о присвоении деятельности юридического лица (ЮЛ) и индивидуального предпринимателя (ИП) определенной категории риска, определенного класса (категории опасности), об отнесении объекта государственного контроля (надзора) к определенной категории ",
                                            id: "informatsiya_o_prisvoenii_deyatelnosti_yuridicheskogo_litsa_yul_i_individualnogo_predprinimatelya_ip",
                                            accessor: d => d.informatsiya_o_prisvoenii_deyatelnosti_yuridicheskogo_litsa_yul_i_individualnogo_predprinimatelya_ip,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["informatsiya_o_prisvoenii_deyatelnosti_yuridicheskogo_litsa_yul_i_individualnogo_predprinimatelya_ip"] }),
                                            filterAll: true
                                        }
                                    ]
                                }, {
                                    columns: [
                                        {
                                            Header: "НОМЕР ПРОВЕРКИ В СИСТЕМЕ АС СППиВП",
                                            id: "nomer_proverki_v_sisteme_as_sppivp",
                                            accessor: d => d.nomer_proverki_v_sisteme_as_sppivp,
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["nomer_proverki_v_sisteme_as_sppivp"] }),
                                            filterAll: true
                                        }
                                    ]
                                }]}
                />
            </div>
        )
    }
}