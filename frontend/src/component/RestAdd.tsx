import * as React from 'react';
import {Entity} from "../api/Api";
import {Form} from "semantic-ui-react";

class RestAddState {
    public entity: Entity;

    constructor(entity: Entity) {
        this.entity = entity;
    }
}

export class RestAdd extends React.Component<{}, RestAddState> {
    constructor(props: {}, context: any) {
        super(props, context);
        this.state = new RestAddState(new Entity());
    }

    bindChange(name: string) {
        return (e, {value}) => {
            this.state[name] = value;
        };
    }

    render() {
        return <Form>
            <Form.Field>
                <label>First Name</label>
                {/*<input placeholder='First Name' onChange={this.bindChange("naimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_munitsipalnogo_kontrolya")}/>*/}
            </Form.Field>
        </Form>;
    }
}