export class Entity {
    public id: number;
    public naimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_munitsipalnogo_kontrolya: string;
    public naimenovanie_yuridicheskogo_litsa_filiala_predstavitelstva_obosoblennogo_strukturnogo_podrazdeleniya: string;
    public adres_mesta_nakhozhdeniya_yul: string;
    public adres_mesta_fakticheskogo_osushchestvleniya_deyatelnosti_yul_ip: string;
    public adres_mesta_nakhozhdeniya_obektov: string;
    public osnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn: string;
    public identifikatsionnyy_nomer_nalogoplatelshchika_inn: string;
    public tsel_provedeniya_proverki: string;
    public osnovanie_provedeniya_proverki_data_gosudarstvennoy_registratsii_yul_ip: string;
    public osnovanie_provedeniya_proverki_data_okonchaniya_posledney_proverki: string;
    public osnovanie_provedeniya_proverki_data_nachala_osushchestvleniya_yul_ip_deyatelnosti_v_sootvetstvii_s_p: string;
    public osnovanie_provedeniya_proverki_inye_osnovaniya_v_sootvetstvii_s_federalnym_zakonom: string;
    public data_nachala_provedeniya_proverki: string;
    public srok_provedeniya_planovoy_proverki_rabochikh_dney: string;
    public srok_provedeniya_planovoy_proverki_rabochikh_chasov_dlya_msp_i_mkp: string;
    public forma_provedeniya_proverki_dokumentarnaya_vyezdnaya_dokumentarnaya_i_vyezdnaya: string;
    public naimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_organa_munitsipalnogo_kontrolya_s_kotorym_pro: string;
    public informatsiya_o_postanovlenii_o_naznachenii_administrativnogo_naznacheniya_ili_reshenii_o_priostanovl: string;
    public informatsiya_o_prisvoenii_deyatelnosti_yuridicheskogo_litsa_yul_i_individualnogo_predprinimatelya_ip: string;
    public nomer_proverki_v_sisteme_as_sppivp: string;

}

export class AlgoEntity {
    public id: number;
    public nomer_proverki_v_sisteme_as_sppivp: string;
    public osnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn: string;
    public identifikatsionnyy_nomer_nalogoplatelshchika_inn: string;
}

export class CryptEntity {
    public id: number;
    public in: string;
    public coded: string;
    public decoded: string;
}

export const errorHandlers: ((reason: string) => void)[] = [];

export class Api {
    public isLoggedIn: boolean;
    public name: string;

    constructor() {
        this.isLoggedIn = false;
        this.name = "";
    }

    static register(login: string, password: string, email: string): Promise<undefined> {
        return fetch("/api/register?login=" + login + "&password=" + password + "&email=" + email)
            .then(value => {
                if(!value.ok)
                    throw new Error(value.statusText);
                return undefined;
            })
    }

    static restore(login: string): Promise<undefined> {
        return fetch("/api/restore?login=" + login)
            .then(value => {
                if(!value.ok)
                    throw new Error(value.statusText);
                return undefined;
            })
    }

    login(login: string, password: string): Promise<undefined> {
        return fetch("/api/login?username=" + login + "&password=" + password)
            .then(value => {
                if(!value.ok)
                    throw new Error(value.statusText);
                return value.text().then(value1 => {
                    this.name = value1;
                    this.isLoggedIn = true;
                    return undefined;
                });
            })
    }

    logout() {
        return fetch("/api/logout")
            .then((value) => {
                if(!value.ok)
                    throw new Error(value.statusText);
            }).catch(reason => {
                errorHandlers.forEach(value => value(reason));
            });
    }

    static query(): Promise<Entity[]> {
        return fetch("/api/public/data")
            .then((value) => {
                if(!value.ok)
                    throw new Error(value.statusText);
                return value.json();
            }).catch(reason => {
                errorHandlers.forEach(value => value(reason));
            });
    }

    static queryAlgo(): Promise<AlgoEntity[]> {
        return fetch("/api/public/algo")
            .then((value) => {
                if(!value.ok)
                    throw new Error(value.statusText);
                return value.json();
            }).catch(reason => {
                errorHandlers.forEach(value => value(reason));
            });
    }

    static queryCrypt(): Promise<CryptEntity[]> {
        return fetch("/api/public/crypt")
            .then((value) => {
                if(!value.ok)
                    throw new Error(value.statusText);
                return value.json();
            }).catch(reason => {
                errorHandlers.forEach(value => value(reason));
            });
    }
}