DROP FUNCTION get_data(registr VARCHAR);

CREATE OR REPLACE FUNCTION get_data(registr TEXT) RETURNS TABLE (osn VARCHAR(2048), tg VARCHAR(255)) AS $$
BEGIN
RETURN QUERY SELECT osnovanie_provedeniya_proverki_data_gosudarstvennoy_registratsi as osn, target as tg
    FROM the_entity LEFT JOIN scan_target_entity ste on the_entity.tsel_provedeniya_proverki_id = ste.id
    WHERE osnovanie_provedeniya_proverki_data_gosudarstvennoy_registratsi = registr
    LIMIT 10;
END; $$ LANGUAGE 'plpgsql';

select * from get_data('13.11.1998');