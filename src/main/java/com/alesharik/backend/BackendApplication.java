package com.alesharik.backend;

import com.alesharik.backend.auth.data.UserAuth;
import com.alesharik.backend.auth.data.UserAuthRepository;
import com.alesharik.backend.data.*;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

@SpringBootApplication
public class BackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendApplication.class, args);
    }

    @Bean
    public JavaMailSender sender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);

        mailSender.setUsername("dev20192@gmail.com");
        mailSender.setPassword("sukablyat");

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;
    }

    @Autowired
    private ImportEntityRepo repo;

    @Autowired
    private ScanTargetEntityRepo scanTargetEntityRepo;
    @Autowired
    private TheEntityRepo entityRepo;
    @Autowired
    private UserAuthRepository authRepository;

    @Bean
    public CommandLineRunner check() {
        return args -> {
            if(authRepository.count() == 0) {
                UserAuth auth = new UserAuth("admin", "alesharik4@gmail.com", "admin");
                authRepository.save(auth);
            }
        };
    }
//
//    @Bean
//    public CommandLineRunner run() {
//        return args -> {
//            CSVParser parser = CSVParser.parse(new File("./data.csv"), StandardCharsets.UTF_8, CSVFormat.DEFAULT.withDelimiter(';').withHeader("naimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_munitsipalnogo_kontrolya",
//                    "naimenovanie_yuridicheskogo_litsa_filiala_predstavitelstva_obosoblennogo_strukturnogo_podrazdeleniya",
//                    "adres_mesta_nakhozhdeniya_yul",
//                    "adres_mesta_fakticheskogo_osushchestvleniya_deyatelnosti_yul_ip",
//                    "adres_mesta_nakhozhdeniya_obektov",
//                    "osnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn",
//                    "identifikatsionnyy_nomer_nalogoplatelshchika_inn",
//                    "tsel_provedeniya_proverki",
//                    "osnovanie_provedeniya_proverki_data_gosudarstvennoy_registratsii_yul_ip",
//                    "osnovanie_provedeniya_proverki_data_okonchaniya_posledney_proverki",
//                    "osnovanie_provedeniya_proverki_data_nachala_osushchestvleniya_yul_ip_deyatelnosti_v_sootvetstvii_s_p",
//                    "osnovanie_provedeniya_proverki_inye_osnovaniya_v_sootvetstvii_s_federalnym_zakonom",
//                    "data_nachala_provedeniya_proverki",
//                    "srok_provedeniya_planovoy_proverki_rabochikh_dney",
//                    "srok_provedeniya_planovoy_proverki_rabochikh_chasov_dlya_msp_i_mkp",
//                    "forma_provedeniya_proverki_dokumentarnaya_vyezdnaya_dokumentarnaya_i_vyezdnaya",
//                    "naimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_organa_munitsipalnogo_kontrolya_s_kotorym_pro",
//                    "informatsiya_o_postanovlenii_o_naznachenii_administrativnogo_naznacheniya_ili_reshenii_o_priostanovl",
//                    "a",
//                    "b",
//                    "informatsiya_o_prisvoenii_deyatelnosti_yuridicheskogo_litsa_yul_i_individualnogo_predprinimatelya_ip",
//                    "nomer_proverki_v_sisteme_as_sppivp"));
//            for (CSVRecord strings : parser) {
//                ImportEntity entity = new ImportEntity();
//                entity.setNaimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_munitsipalnogo_kontrolya(strings.get("naimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_munitsipalnogo_kontrolya"));
//                entity.setNaimenovanie_yuridicheskogo_litsa_filiala_predstavitelstva_obosoblennogo_strukturnogo_podrazdeleniya(strings.get("naimenovanie_yuridicheskogo_litsa_filiala_predstavitelstva_obosoblennogo_strukturnogo_podrazdeleniya"));
//                entity.setAdres_mesta_nakhozhdeniya_yul(strings.get("adres_mesta_nakhozhdeniya_yul"));
//                entity.setAdres_mesta_fakticheskogo_osushchestvleniya_deyatelnosti_yul_ip(strings.get("adres_mesta_fakticheskogo_osushchestvleniya_deyatelnosti_yul_ip"));
//                entity.setAdres_mesta_nakhozhdeniya_obektov(strings.get("adres_mesta_nakhozhdeniya_obektov"));
//                entity.setOsnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn(strings.get("osnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn"));
//                entity.setIdentifikatsionnyy_nomer_nalogoplatelshchika_inn(strings.get("identifikatsionnyy_nomer_nalogoplatelshchika_inn"));
//                entity.setTsel_provedeniya_proverki(strings.get("tsel_provedeniya_proverki"));
//                entity.setOsnovanie_provedeniya_proverki_data_gosudarstvennoy_registratsii_yul_ip(strings.get("osnovanie_provedeniya_proverki_data_gosudarstvennoy_registratsii_yul_ip"));
//                entity.setOsnovanie_provedeniya_proverki_data_okonchaniya_posledney_proverki(strings.get("osnovanie_provedeniya_proverki_data_okonchaniya_posledney_proverki"));
//                entity.setOsnovanie_provedeniya_proverki_data_nachala_osushchestvleniya_yul_ip_deyatelnosti_v_sootvetstvii_s_p(strings.get("osnovanie_provedeniya_proverki_data_nachala_osushchestvleniya_yul_ip_deyatelnosti_v_sootvetstvii_s_p"));
//                entity.setOsnovanie_provedeniya_proverki_inye_osnovaniya_v_sootvetstvii_s_federalnym_zakonom(strings.get("osnovanie_provedeniya_proverki_inye_osnovaniya_v_sootvetstvii_s_federalnym_zakonom"));
//                entity.setData_nachala_provedeniya_proverki(strings.get("data_nachala_provedeniya_proverki"));
//                entity.setSrok_provedeniya_planovoy_proverki_rabochikh_dney(strings.get("srok_provedeniya_planovoy_proverki_rabochikh_dney"));
//                entity.setSrok_provedeniya_planovoy_proverki_rabochikh_chasov_dlya_msp_i_mkp(strings.get("srok_provedeniya_planovoy_proverki_rabochikh_chasov_dlya_msp_i_mkp"));
//                entity.setForma_provedeniya_proverki_dokumentarnaya_vyezdnaya_dokumentarnaya_i_vyezdnaya(strings.get("forma_provedeniya_proverki_dokumentarnaya_vyezdnaya_dokumentarnaya_i_vyezdnaya"));
//                entity.setNaimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_organa_munitsipalnogo_kontrolya_s_kotorym_pro(strings.get("naimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_organa_munitsipalnogo_kontrolya_s_kotorym_pro"));
//                entity.setInformatsiya_o_postanovlenii_o_naznachenii_administrativnogo_naznacheniya_ili_reshenii_o_priostanovl(strings.get("informatsiya_o_postanovlenii_o_naznachenii_administrativnogo_naznacheniya_ili_reshenii_o_priostanovl"));
//                entity.setInformatsiya_o_prisvoenii_deyatelnosti_yuridicheskogo_litsa_yul_i_individualnogo_predprinimatelya_ip(strings.get("informatsiya_o_prisvoenii_deyatelnosti_yuridicheskogo_litsa_yul_i_individualnogo_predprinimatelya_ip"));
//                entity.setNomer_proverki_v_sisteme_as_sppivp(strings.get("nomer_proverki_v_sisteme_as_sppivp"));
//                repo.save(entity);
//            }
//        };
//    }
//
//    @Bean
//    public CommandLineRunner convert() {
//        return args -> {
//            for (ImportEntity entity : repo.findAll()) {
//                TheEntity e = new TheEntity();
//                e.setNaimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_munitsipalnogo_kontrolya(entity.getNaimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_munitsipalnogo_kontrolya());
//                e.setNaimenovanie_yuridicheskogo_litsa_filiala_predstavitelstva_obosoblennogo_strukturnogo_podrazdeleniya(entity.getNaimenovanie_yuridicheskogo_litsa_filiala_predstavitelstva_obosoblennogo_strukturnogo_podrazdeleniya());
//                e.setAdres_mesta_nakhozhdeniya_yul(entity.getAdres_mesta_nakhozhdeniya_yul());
//                e.setAdres_mesta_fakticheskogo_osushchestvleniya_deyatelnosti_yul_ip(entity.getAdres_mesta_fakticheskogo_osushchestvleniya_deyatelnosti_yul_ip());
//                e.setAdres_mesta_nakhozhdeniya_obektov(entity.getAdres_mesta_nakhozhdeniya_obektov());
//                e.setOsnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn(entity.getOsnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn());
//                e.setIdentifikatsionnyy_nomer_nalogoplatelshchika_inn(entity.getIdentifikatsionnyy_nomer_nalogoplatelshchika_inn());
//                e.setOsnovanie_provedeniya_proverki_data_gosudarstvennoy_registratsii_yul_ip(entity.getOsnovanie_provedeniya_proverki_data_gosudarstvennoy_registratsii_yul_ip());
//                e.setOsnovanie_provedeniya_proverki_data_okonchaniya_posledney_proverki(entity.getOsnovanie_provedeniya_proverki_data_okonchaniya_posledney_proverki());
//                e.setOsnovanie_provedeniya_proverki_data_nachala_osushchestvleniya_yul_ip_deyatelnosti_v_sootvetstvii_s_p(entity.getOsnovanie_provedeniya_proverki_data_nachala_osushchestvleniya_yul_ip_deyatelnosti_v_sootvetstvii_s_p());
//                e.setOsnovanie_provedeniya_proverki_inye_osnovaniya_v_sootvetstvii_s_federalnym_zakonom(entity.getOsnovanie_provedeniya_proverki_inye_osnovaniya_v_sootvetstvii_s_federalnym_zakonom());
//                e.setData_nachala_provedeniya_proverki(entity.getData_nachala_provedeniya_proverki());
//                e.setSrok_provedeniya_planovoy_proverki_rabochikh_dney(entity.getSrok_provedeniya_planovoy_proverki_rabochikh_dney());
//                e.setSrok_provedeniya_planovoy_proverki_rabochikh_chasov_dlya_msp_i_mkp(entity.getSrok_provedeniya_planovoy_proverki_rabochikh_chasov_dlya_msp_i_mkp());
//                e.setForma_provedeniya_proverki_dokumentarnaya_vyezdnaya_dokumentarnaya_i_vyezdnaya(entity.getForma_provedeniya_proverki_dokumentarnaya_vyezdnaya_dokumentarnaya_i_vyezdnaya());
//                e.setNaimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_organa_munitsipalnogo_kontrolya_s_kotorym_pro(entity.getNaimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_organa_munitsipalnogo_kontrolya_s_kotorym_pro());
//                e.setInformatsiya_o_postanovlenii_o_naznachenii_administrativnogo_naznacheniya_ili_reshenii_o_priostanovl(entity.getInformatsiya_o_postanovlenii_o_naznachenii_administrativnogo_naznacheniya_ili_reshenii_o_priostanovl());
//                e.setInformatsiya_o_prisvoenii_deyatelnosti_yuridicheskogo_litsa_yul_i_individualnogo_predprinimatelya_ip(entity.getInformatsiya_o_prisvoenii_deyatelnosti_yuridicheskogo_litsa_yul_i_individualnogo_predprinimatelya_ip());
//                e.setNomer_proverki_v_sisteme_as_sppivp(entity.getNomer_proverki_v_sisteme_as_sppivp());
//                e.setTsel_provedeniya_proverki(scanTargetEntityRepo.findByTarget(entity.getTsel_provedeniya_proverki()).orElseGet(() -> {
//                    ScanTargetEntity e1 = new ScanTargetEntity();
//                    e1.setTarget(entity.getTsel_provedeniya_proverki());
//                    return scanTargetEntityRepo.save(e1);
//                }));
//                entityRepo.save(e);
//            }
//        };
//    }

}
