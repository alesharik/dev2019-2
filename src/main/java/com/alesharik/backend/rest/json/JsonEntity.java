package com.alesharik.backend.rest.json;

import com.alesharik.backend.data.ScanTargetEntity;
import com.alesharik.backend.data.TheEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Data
public class JsonEntity {
    private long id;
    private String naimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_munitsipalnogo_kontrolya;
    private String naimenovanie_yuridicheskogo_litsa_filiala_predstavitelstva_obosoblennogo_strukturnogo_podrazdeleniya;
    private String adres_mesta_nakhozhdeniya_yul;
    private String adres_mesta_fakticheskogo_osushchestvleniya_deyatelnosti_yul_ip;
    private String adres_mesta_nakhozhdeniya_obektov;
    private String osnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn;
    private String identifikatsionnyy_nomer_nalogoplatelshchika_inn;
    private String tsel_provedeniya_proverki;
    private String osnovanie_provedeniya_proverki_data_gosudarstvennoy_registratsii_yul_ip;
    private String osnovanie_provedeniya_proverki_data_okonchaniya_posledney_proverki;
    private String osnovanie_provedeniya_proverki_data_nachala_osushchestvleniya_yul_ip_deyatelnosti_v_sootvetstvii_s_p;
    private String osnovanie_provedeniya_proverki_inye_osnovaniya_v_sootvetstvii_s_federalnym_zakonom;
    private String data_nachala_provedeniya_proverki;
    private String srok_provedeniya_planovoy_proverki_rabochikh_dney;
    private String srok_provedeniya_planovoy_proverki_rabochikh_chasov_dlya_msp_i_mkp;
    private String forma_provedeniya_proverki_dokumentarnaya_vyezdnaya_dokumentarnaya_i_vyezdnaya;
    private String naimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_organa_munitsipalnogo_kontrolya_s_kotorym_pro;
    private String informatsiya_o_postanovlenii_o_naznachenii_administrativnogo_naznacheniya_ili_reshenii_o_priostanovl;
    private String informatsiya_o_prisvoenii_deyatelnosti_yuridicheskogo_litsa_yul_i_individualnogo_predprinimatelya_ip;
    private String nomer_proverki_v_sisteme_as_sppivp;

    public JsonEntity(TheEntity entity) {
        this.id = entity.getId();
        setNaimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_munitsipalnogo_kontrolya(entity.getNaimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_munitsipalnogo_kontrolya());
        setNaimenovanie_yuridicheskogo_litsa_filiala_predstavitelstva_obosoblennogo_strukturnogo_podrazdeleniya(entity.getNaimenovanie_yuridicheskogo_litsa_filiala_predstavitelstva_obosoblennogo_strukturnogo_podrazdeleniya());
        setAdres_mesta_nakhozhdeniya_yul(entity.getAdres_mesta_nakhozhdeniya_yul());
        setAdres_mesta_fakticheskogo_osushchestvleniya_deyatelnosti_yul_ip(entity.getAdres_mesta_fakticheskogo_osushchestvleniya_deyatelnosti_yul_ip());
        setAdres_mesta_nakhozhdeniya_obektov(entity.getAdres_mesta_nakhozhdeniya_obektov());
        setOsnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn(entity.getOsnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn());
        setIdentifikatsionnyy_nomer_nalogoplatelshchika_inn(entity.getIdentifikatsionnyy_nomer_nalogoplatelshchika_inn());
        setOsnovanie_provedeniya_proverki_data_gosudarstvennoy_registratsii_yul_ip(entity.getOsnovanie_provedeniya_proverki_data_gosudarstvennoy_registratsii_yul_ip());
        setOsnovanie_provedeniya_proverki_data_okonchaniya_posledney_proverki(entity.getOsnovanie_provedeniya_proverki_data_okonchaniya_posledney_proverki());
        setOsnovanie_provedeniya_proverki_data_nachala_osushchestvleniya_yul_ip_deyatelnosti_v_sootvetstvii_s_p(entity.getOsnovanie_provedeniya_proverki_data_nachala_osushchestvleniya_yul_ip_deyatelnosti_v_sootvetstvii_s_p());
        setOsnovanie_provedeniya_proverki_inye_osnovaniya_v_sootvetstvii_s_federalnym_zakonom(entity.getOsnovanie_provedeniya_proverki_inye_osnovaniya_v_sootvetstvii_s_federalnym_zakonom());
        setData_nachala_provedeniya_proverki(entity.getData_nachala_provedeniya_proverki());
        setSrok_provedeniya_planovoy_proverki_rabochikh_dney(entity.getSrok_provedeniya_planovoy_proverki_rabochikh_dney());
        setSrok_provedeniya_planovoy_proverki_rabochikh_chasov_dlya_msp_i_mkp(entity.getSrok_provedeniya_planovoy_proverki_rabochikh_chasov_dlya_msp_i_mkp());
        setForma_provedeniya_proverki_dokumentarnaya_vyezdnaya_dokumentarnaya_i_vyezdnaya(entity.getForma_provedeniya_proverki_dokumentarnaya_vyezdnaya_dokumentarnaya_i_vyezdnaya());
        setNaimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_organa_munitsipalnogo_kontrolya_s_kotorym_pro(entity.getNaimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_organa_munitsipalnogo_kontrolya_s_kotorym_pro());
        setInformatsiya_o_postanovlenii_o_naznachenii_administrativnogo_naznacheniya_ili_reshenii_o_priostanovl(entity.getInformatsiya_o_postanovlenii_o_naznachenii_administrativnogo_naznacheniya_ili_reshenii_o_priostanovl());
        setInformatsiya_o_prisvoenii_deyatelnosti_yuridicheskogo_litsa_yul_i_individualnogo_predprinimatelya_ip(entity.getInformatsiya_o_prisvoenii_deyatelnosti_yuridicheskogo_litsa_yul_i_individualnogo_predprinimatelya_ip());
        setNomer_proverki_v_sisteme_as_sppivp(entity.getNomer_proverki_v_sisteme_as_sppivp());
        setTsel_provedeniya_proverki(entity.getTsel_provedeniya_proverki().getTarget());
    }
}
