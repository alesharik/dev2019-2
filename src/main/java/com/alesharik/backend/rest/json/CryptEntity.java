package com.alesharik.backend.rest.json;

import lombok.Data;

import static com.alesharik.backend.CryptoUtils.decrypt;
import static com.alesharik.backend.CryptoUtils.encrypt;

@Data
public class CryptEntity {
    private long id;
    private String in;
    private String coded;
    private String decoded;

    public CryptEntity(long id, String in) {
        this.id = id;
        this.in = in;
        this.coded = encrypt(in, "ТЕСТ");
        this.decoded = decrypt(coded, "ВЕБТУЛА");
    }

}
