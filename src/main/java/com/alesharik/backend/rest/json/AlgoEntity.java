package com.alesharik.backend.rest.json;

import com.alesharik.backend.data.TheEntity;
import lombok.Data;

@Data
public class AlgoEntity {
    private long id;
    private String nomer_proverki_v_sisteme_as_sppivp;
    private String osnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn;
    private String identifikatsionnyy_nomer_nalogoplatelshchika_inn;

    public AlgoEntity(TheEntity entity) {
        this.id = entity.getId();
        this.nomer_proverki_v_sisteme_as_sppivp = entity.getNomer_proverki_v_sisteme_as_sppivp();
        this.osnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn = entity.getOsnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn();
        this.identifikatsionnyy_nomer_nalogoplatelshchika_inn = entity.getIdentifikatsionnyy_nomer_nalogoplatelshchika_inn();
    }
}
