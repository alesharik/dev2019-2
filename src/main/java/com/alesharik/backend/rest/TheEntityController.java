package com.alesharik.backend.rest;

import com.alesharik.backend.data.ScanTargetEntity;
import com.alesharik.backend.data.ScanTargetEntityRepo;
import com.alesharik.backend.data.TheEntity;
import com.alesharik.backend.data.TheEntityRepo;
import com.alesharik.backend.rest.json.JsonEntity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class TheEntityController {
    private static final Gson GSON = new GsonBuilder().create();

    private final ScanTargetEntityRepo scanTargetEntityRepo;
    private final TheEntityRepo entityRepo;

    @PostMapping("/api/data")
    public ResponseEntity<JsonEntity> post(@RequestBody String json) {
        JsonEntity entity = GSON.fromJson(json, JsonEntity.class);
        TheEntity e = new TheEntity();
        e.setNaimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_munitsipalnogo_kontrolya(entity.getNaimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_munitsipalnogo_kontrolya());
        e.setNaimenovanie_yuridicheskogo_litsa_filiala_predstavitelstva_obosoblennogo_strukturnogo_podrazdeleniya(entity.getNaimenovanie_yuridicheskogo_litsa_filiala_predstavitelstva_obosoblennogo_strukturnogo_podrazdeleniya());
        e.setAdres_mesta_nakhozhdeniya_yul(entity.getAdres_mesta_nakhozhdeniya_yul());
        e.setAdres_mesta_fakticheskogo_osushchestvleniya_deyatelnosti_yul_ip(entity.getAdres_mesta_fakticheskogo_osushchestvleniya_deyatelnosti_yul_ip());
        e.setAdres_mesta_nakhozhdeniya_obektov(entity.getAdres_mesta_nakhozhdeniya_obektov());
        e.setOsnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn(entity.getOsnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn());
        e.setIdentifikatsionnyy_nomer_nalogoplatelshchika_inn(entity.getIdentifikatsionnyy_nomer_nalogoplatelshchika_inn());
        e.setOsnovanie_provedeniya_proverki_data_gosudarstvennoy_registratsii_yul_ip(entity.getOsnovanie_provedeniya_proverki_data_gosudarstvennoy_registratsii_yul_ip());
        e.setOsnovanie_provedeniya_proverki_data_okonchaniya_posledney_proverki(entity.getOsnovanie_provedeniya_proverki_data_okonchaniya_posledney_proverki());
        e.setOsnovanie_provedeniya_proverki_data_nachala_osushchestvleniya_yul_ip_deyatelnosti_v_sootvetstvii_s_p(entity.getOsnovanie_provedeniya_proverki_data_nachala_osushchestvleniya_yul_ip_deyatelnosti_v_sootvetstvii_s_p());
        e.setOsnovanie_provedeniya_proverki_inye_osnovaniya_v_sootvetstvii_s_federalnym_zakonom(entity.getOsnovanie_provedeniya_proverki_inye_osnovaniya_v_sootvetstvii_s_federalnym_zakonom());
        e.setData_nachala_provedeniya_proverki(entity.getData_nachala_provedeniya_proverki());
        e.setSrok_provedeniya_planovoy_proverki_rabochikh_dney(entity.getSrok_provedeniya_planovoy_proverki_rabochikh_dney());
        e.setSrok_provedeniya_planovoy_proverki_rabochikh_chasov_dlya_msp_i_mkp(entity.getSrok_provedeniya_planovoy_proverki_rabochikh_chasov_dlya_msp_i_mkp());
        e.setForma_provedeniya_proverki_dokumentarnaya_vyezdnaya_dokumentarnaya_i_vyezdnaya(entity.getForma_provedeniya_proverki_dokumentarnaya_vyezdnaya_dokumentarnaya_i_vyezdnaya());
        e.setNaimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_organa_munitsipalnogo_kontrolya_s_kotorym_pro(entity.getNaimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_organa_munitsipalnogo_kontrolya_s_kotorym_pro());
        e.setInformatsiya_o_postanovlenii_o_naznachenii_administrativnogo_naznacheniya_ili_reshenii_o_priostanovl(entity.getInformatsiya_o_postanovlenii_o_naznachenii_administrativnogo_naznacheniya_ili_reshenii_o_priostanovl());
        e.setInformatsiya_o_prisvoenii_deyatelnosti_yuridicheskogo_litsa_yul_i_individualnogo_predprinimatelya_ip(entity.getInformatsiya_o_prisvoenii_deyatelnosti_yuridicheskogo_litsa_yul_i_individualnogo_predprinimatelya_ip());
        e.setNomer_proverki_v_sisteme_as_sppivp(entity.getNomer_proverki_v_sisteme_as_sppivp());
        e.setTsel_provedeniya_proverki(scanTargetEntityRepo.findByTarget(entity.getTsel_provedeniya_proverki()).orElseGet(() -> {
            ScanTargetEntity e1 = new ScanTargetEntity();
            e1.setTarget(entity.getTsel_provedeniya_proverki());
            return scanTargetEntityRepo.save(e1);
        }));
        return new ResponseEntity<>(new JsonEntity(entityRepo.save(e)), HttpStatus.OK);
    }

    @GetMapping("/api/data")
    public ResponseEntity<List<JsonEntity>> get() {
        List<JsonEntity> entities = new ArrayList<>();
        for (TheEntity theEntity : entityRepo.findAll()) {
            entities.add(new JsonEntity(theEntity));
        }
        return new ResponseEntity<>(entities, HttpStatus.OK);
    }

    @GetMapping("/api/public/data")
    public ResponseEntity<List<JsonEntity>> getPublic() {
        List<JsonEntity> entities = new ArrayList<>();
        for (TheEntity theEntity : entityRepo.findAll()) {
            entities.add(new JsonEntity(theEntity));
        }
        return new ResponseEntity<>(entities, HttpStatus.OK);
    }

    @PutMapping("/api/data")
    public ResponseEntity<JsonEntity> update(@RequestParam("id") long id, @RequestBody String json) {
        JsonEntity entity = GSON.fromJson(json, JsonEntity.class);
        TheEntity e = entityRepo.findById(id).orElseThrow(IllegalArgumentException::new);
        if(entity.getNaimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_munitsipalnogo_kontrolya() != null)
            e.setNaimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_munitsipalnogo_kontrolya(entity.getNaimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_munitsipalnogo_kontrolya());
        if(entity.getNaimenovanie_yuridicheskogo_litsa_filiala_predstavitelstva_obosoblennogo_strukturnogo_podrazdeleniya() != null)
            e.setNaimenovanie_yuridicheskogo_litsa_filiala_predstavitelstva_obosoblennogo_strukturnogo_podrazdeleniya(entity.getNaimenovanie_yuridicheskogo_litsa_filiala_predstavitelstva_obosoblennogo_strukturnogo_podrazdeleniya());
        if(entity.getAdres_mesta_nakhozhdeniya_yul() != null)
            e.setAdres_mesta_nakhozhdeniya_yul(entity.getAdres_mesta_nakhozhdeniya_yul());
        if(entity.getAdres_mesta_fakticheskogo_osushchestvleniya_deyatelnosti_yul_ip() != null)
            e.setAdres_mesta_fakticheskogo_osushchestvleniya_deyatelnosti_yul_ip(entity.getAdres_mesta_fakticheskogo_osushchestvleniya_deyatelnosti_yul_ip());
        if(entity.getAdres_mesta_nakhozhdeniya_obektov() != null)
            e.setAdres_mesta_nakhozhdeniya_obektov(entity.getAdres_mesta_nakhozhdeniya_obektov());
        if(entity.getOsnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn() != null)
            e.setOsnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn(entity.getOsnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn());
        if(entity.getIdentifikatsionnyy_nomer_nalogoplatelshchika_inn() != null)
            e.setIdentifikatsionnyy_nomer_nalogoplatelshchika_inn(entity.getIdentifikatsionnyy_nomer_nalogoplatelshchika_inn());
        if(entity.getOsnovanie_provedeniya_proverki_data_gosudarstvennoy_registratsii_yul_ip() != null)
            e.setOsnovanie_provedeniya_proverki_data_gosudarstvennoy_registratsii_yul_ip(entity.getOsnovanie_provedeniya_proverki_data_gosudarstvennoy_registratsii_yul_ip());
        if(entity.getOsnovanie_provedeniya_proverki_data_okonchaniya_posledney_proverki() != null)
            e.setOsnovanie_provedeniya_proverki_data_okonchaniya_posledney_proverki(entity.getOsnovanie_provedeniya_proverki_data_okonchaniya_posledney_proverki());
        if(entity.getOsnovanie_provedeniya_proverki_data_nachala_osushchestvleniya_yul_ip_deyatelnosti_v_sootvetstvii_s_p() != null)
            e.setOsnovanie_provedeniya_proverki_data_nachala_osushchestvleniya_yul_ip_deyatelnosti_v_sootvetstvii_s_p(entity.getOsnovanie_provedeniya_proverki_data_nachala_osushchestvleniya_yul_ip_deyatelnosti_v_sootvetstvii_s_p());
        if(entity.getOsnovanie_provedeniya_proverki_inye_osnovaniya_v_sootvetstvii_s_federalnym_zakonom() != null)
            e.setOsnovanie_provedeniya_proverki_inye_osnovaniya_v_sootvetstvii_s_federalnym_zakonom(entity.getOsnovanie_provedeniya_proverki_inye_osnovaniya_v_sootvetstvii_s_federalnym_zakonom());
        if(entity.getData_nachala_provedeniya_proverki() != null)
            e.setData_nachala_provedeniya_proverki(entity.getData_nachala_provedeniya_proverki());
        if(entity.getSrok_provedeniya_planovoy_proverki_rabochikh_dney() != null)
            e.setSrok_provedeniya_planovoy_proverki_rabochikh_dney(entity.getSrok_provedeniya_planovoy_proverki_rabochikh_dney());
        if(entity.getSrok_provedeniya_planovoy_proverki_rabochikh_chasov_dlya_msp_i_mkp() != null)
            e.setSrok_provedeniya_planovoy_proverki_rabochikh_chasov_dlya_msp_i_mkp(entity.getSrok_provedeniya_planovoy_proverki_rabochikh_chasov_dlya_msp_i_mkp());
        if(entity.getForma_provedeniya_proverki_dokumentarnaya_vyezdnaya_dokumentarnaya_i_vyezdnaya() != null)
            e.setForma_provedeniya_proverki_dokumentarnaya_vyezdnaya_dokumentarnaya_i_vyezdnaya(entity.getForma_provedeniya_proverki_dokumentarnaya_vyezdnaya_dokumentarnaya_i_vyezdnaya());
        if(entity.getNaimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_organa_munitsipalnogo_kontrolya_s_kotorym_pro() != null)
            e.setNaimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_organa_munitsipalnogo_kontrolya_s_kotorym_pro(entity.getNaimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_organa_munitsipalnogo_kontrolya_s_kotorym_pro());
        if(entity.getInformatsiya_o_postanovlenii_o_naznachenii_administrativnogo_naznacheniya_ili_reshenii_o_priostanovl() != null)
            e.setInformatsiya_o_postanovlenii_o_naznachenii_administrativnogo_naznacheniya_ili_reshenii_o_priostanovl(entity.getInformatsiya_o_postanovlenii_o_naznachenii_administrativnogo_naznacheniya_ili_reshenii_o_priostanovl());
        if(entity.getInformatsiya_o_prisvoenii_deyatelnosti_yuridicheskogo_litsa_yul_i_individualnogo_predprinimatelya_ip() != null)
            e.setInformatsiya_o_prisvoenii_deyatelnosti_yuridicheskogo_litsa_yul_i_individualnogo_predprinimatelya_ip(entity.getInformatsiya_o_prisvoenii_deyatelnosti_yuridicheskogo_litsa_yul_i_individualnogo_predprinimatelya_ip());
        if(entity.getNomer_proverki_v_sisteme_as_sppivp() != null)
            e.setNomer_proverki_v_sisteme_as_sppivp(entity.getNomer_proverki_v_sisteme_as_sppivp());
        if(entity.getTsel_provedeniya_proverki() != null && !e.getTsel_provedeniya_proverki().getTarget().equals(entity.getTsel_provedeniya_proverki())) {
            ScanTargetEntity last = e.getTsel_provedeniya_proverki();
            if(last.getOwners().size() == 1)
                scanTargetEntityRepo.delete(last);
            e.setTsel_provedeniya_proverki(scanTargetEntityRepo.findByTarget(entity.getTsel_provedeniya_proverki()).orElseGet(() -> {
                ScanTargetEntity e1 = new ScanTargetEntity();
                e1.setTarget(entity.getTsel_provedeniya_proverki());
                return scanTargetEntityRepo.save(e1);
            }));
        }
        return new ResponseEntity<>(new JsonEntity(entityRepo.save(e)), HttpStatus.OK);
    }

    @DeleteMapping("/api/data")
    public ResponseEntity<JsonEntity> delete(@RequestParam("id") long id) {
        TheEntity e = entityRepo.findById(id).orElseThrow(IllegalArgumentException::new);
        if(e.getTsel_provedeniya_proverki().getOwners().size() == 1)
            scanTargetEntityRepo.delete(e.getTsel_provedeniya_proverki());
        else {
            ScanTargetEntity target = e.getTsel_provedeniya_proverki();
            target.getOwners().remove(e);
            scanTargetEntityRepo.save(target);
        }
        JsonEntity entity = new JsonEntity(e);
        entityRepo.delete(e);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }
}
