package com.alesharik.backend.rest;

import com.alesharik.backend.data.TheEntity;
import com.alesharik.backend.data.TheEntityRepo;
import com.alesharik.backend.rest.json.CryptEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class CryptController {
    private final TheEntityRepo repo;

    @GetMapping("/api/public/crypt")
    public ResponseEntity<List<CryptEntity>> get() {
        List<CryptEntity> entities = new ArrayList<>();
        for (TheEntity theEntity : repo.findAll())
            entities.add(new CryptEntity(theEntity.getId(), theEntity.getNaimenovanie_yuridicheskogo_litsa_filiala_predstavitelstva_obosoblennogo_strukturnogo_podrazdeleniya()));
        return new ResponseEntity<>(entities, HttpStatus.OK);
    }
}
