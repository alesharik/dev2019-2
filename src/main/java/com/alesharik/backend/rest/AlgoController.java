package com.alesharik.backend.rest;

import com.alesharik.backend.data.TheEntity;
import com.alesharik.backend.data.TheEntityRepo;
import com.alesharik.backend.rest.json.AlgoEntity;
import com.alesharik.backend.rest.json.JsonEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class AlgoController {
    private final TheEntityRepo repo;

    @GetMapping("/api/public/algo")
    public ResponseEntity<List<AlgoEntity>> sort() {
        Iterable<TheEntity> all = repo.findAll();
        List<TheEntity> entities = new ArrayList<>();
        all.forEach(entities::add);
        TheEntity[] a1 = entities.toArray(new TheEntity[0]);
        Arrays.sort(a1, (a, b) -> {
            int firstCmp = Long.compare(Long.parseLong(a.getNomer_proverki_v_sisteme_as_sppivp()), Long.parseLong(b.getNomer_proverki_v_sisteme_as_sppivp()));
            if(firstCmp == 0) {
                int secondCmp = Long.compare(Long.parseLong(a.getIdentifikatsionnyy_nomer_nalogoplatelshchika_inn()), Long.parseLong(b.getIdentifikatsionnyy_nomer_nalogoplatelshchika_inn()));
                if(secondCmp == 0)
                    return Long.compare(Long.parseLong(a.getOsnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn()), Long.parseLong(b.getOsnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn()));
                else
                    return secondCmp * -1;
            } else
                return firstCmp;
        });
        List<TheEntity> sorted = Arrays.asList(a1);
        return new ResponseEntity<>(sorted.stream()
                .map(AlgoEntity::new)
                .collect(Collectors.toList()),
                HttpStatus.OK);
    }
}
