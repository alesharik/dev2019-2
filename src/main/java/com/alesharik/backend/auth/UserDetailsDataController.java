package com.alesharik.backend.auth;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserDetailsDataController {
    @GetMapping("/api/user")
    public String login() {
        return (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
