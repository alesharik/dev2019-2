package com.alesharik.backend.auth;

import com.alesharik.backend.auth.data.UserAuth;
import com.alesharik.backend.auth.data.UserAuthRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ThreadLocalRandom;

@RestController
@RequiredArgsConstructor
public class RestoreController {
    private final UserAuthRepository repository;
    private final JavaMailSender sender;

    @GetMapping("/api/restore")
    public void restore(@RequestParam("login") String login) {
        UserAuth auth = repository.findUserAuthByLogin(login).orElseThrow(IllegalArgumentException::new);
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(auth.getEmail());
        message.setSubject("Password restore");
        String pass = randomString();
        auth.updatePassword(pass);
        repository.save(auth);
        message.setText("Your password is " + pass);
        sender.send(message);
    }

    private static String randomString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 24; i++) {
            builder.append(((char) ThreadLocalRandom.current().nextInt('a', 'z')));
        }
        return builder.toString();
    }
}
