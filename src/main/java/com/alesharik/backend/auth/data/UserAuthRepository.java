package com.alesharik.backend.auth.data;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserAuthRepository extends CrudRepository<UserAuth, Long> {

    @Query("select auth from UserAuth auth where auth.login = :login")
    Optional<UserAuth> findUserAuthByLogin(@Param("login") String login);

    @Query("select COUNT (auth) from UserAuth auth WHERE auth.login = :login")
    long countAllByLogin(@Param("login") String login);
}
