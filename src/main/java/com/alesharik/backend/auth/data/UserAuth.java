package com.alesharik.backend.auth.data;

import com.alesharik.backend.auth.Security;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class UserAuth {
    @Id
    @GeneratedValue
    private long id;

    private String password;
    private String email;
    private String login;

    public UserAuth() {
        email = null;
        password = "";
        login = "";
    }

    public UserAuth(String password, String email, String login) {
        this.email = email;
        this.password = Security.getPasswordEncoder().encode(password);
        this.login = login;
    }

    public void updatePassword(String password) {
        this.password = Security.getPasswordEncoder().encode(password);
    }
}
