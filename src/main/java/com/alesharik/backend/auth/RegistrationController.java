package com.alesharik.backend.auth;

import com.alesharik.backend.auth.data.UserAuth;
import com.alesharik.backend.auth.data.UserAuthRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Slf4j
public class RegistrationController {
    private final UserAuthRepository authRepository;

    @GetMapping("/register")
    public void register(@RequestParam("login") String login, @RequestParam("password") String password, @RequestParam("email") String email) {
        if(StringUtils.isEmpty(login) || StringUtils.isEmpty(password))
            throw new IllegalArgumentException("Login or password is empty");
        if(authRepository.countAllByLogin(login) > 0)
            throw new IllegalArgumentException("User already exists");

        log.debug("Registering user " + login + " with password " + password);
        UserAuth auth = new UserAuth(password, email, login);
        authRepository.save(auth);
    }
}
