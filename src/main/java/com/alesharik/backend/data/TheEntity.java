package com.alesharik.backend.data;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(indexes = {@Index(columnList = "identifikatsionnyy_nomer_nalogoplatelshchika_inn", name = "identifikatsionnyy_nomer_nalogoplatelshchika_inn")})
public class TheEntity {
    @Id
    @GeneratedValue
    private long id;

    @Column(length = 2048, name = "naimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_organa_m")
    private String naimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_munitsipalnogo_kontrolya;
    @Column(length = 2048, name = "naimenovanie_yuridicheskogo_litsa_filiala_predstavitelstva_obos")
    private String naimenovanie_yuridicheskogo_litsa_filiala_predstavitelstva_obosoblennogo_strukturnogo_podrazdeleniya;
    @Column(length = 2048, name = "adres_mesta_nakhozhdeniya_yul")
    private String adres_mesta_nakhozhdeniya_yul;
    @Column(length = 2048, name = "adres_mesta_fakticheskogo_osushchestvleniya_deyatelnosti_yul_ip")
    private String adres_mesta_fakticheskogo_osushchestvleniya_deyatelnosti_yul_ip;
    @Column(length = 2048, name = "adres_mesta_nakhozhdeniya_obektov")
    private String adres_mesta_nakhozhdeniya_obektov;
    @Column(length = 2048, name = "osnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn")
    private String osnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn;
    @Column(length = 2048, name = "identifikatsionnyy_nomer_nalogoplatelshchika_inn")
    private String identifikatsionnyy_nomer_nalogoplatelshchika_inn;
    @ManyToOne(fetch = FetchType.EAGER)
    private ScanTargetEntity tsel_provedeniya_proverki;
    @Column(length = 2048, name = "osnovanie_provedeniya_proverki_data_gosudarstvennoy_registratsi")
    private String osnovanie_provedeniya_proverki_data_gosudarstvennoy_registratsii_yul_ip;
    @Column(length = 2048, name = "osnovanie_provedeniya_proverki_data_okonchaniya_posledney_prove")
    private String osnovanie_provedeniya_proverki_data_okonchaniya_posledney_proverki;
    @Column(length = 2048, name = "osnovanie_provedeniya_proverki_data_nachala_osushchestvleniya_y")
    private String osnovanie_provedeniya_proverki_data_nachala_osushchestvleniya_yul_ip_deyatelnosti_v_sootvetstvii_s_p;
    @Column(length = 2048, name = "osnovanie_provedeniya_proverki_inye_osnovaniya_v_sootvetstvii_s")
    private String osnovanie_provedeniya_proverki_inye_osnovaniya_v_sootvetstvii_s_federalnym_zakonom;
    @Column(length = 2048, name = "data_nachala_provedeniya_proverki")
    private String data_nachala_provedeniya_proverki;
    @Column(length = 2048, name = "srok_provedeniya_planovoy_proverki_rabochikh_dney")
    private String srok_provedeniya_planovoy_proverki_rabochikh_dney;
    @Column(length = 2048, name = "srok_provedeniya_planovoy_proverki_rabochikh_chasov_dlya_msp_i_")
    private String srok_provedeniya_planovoy_proverki_rabochikh_chasov_dlya_msp_i_mkp;
    @Column(length = 2048, name = "forma_provedeniya_proverki_dokumentarnaya_vyezdnaya_dokumentarn")
    private String forma_provedeniya_proverki_dokumentarnaya_vyezdnaya_dokumentarnaya_i_vyezdnaya;
    @Column(length = 2048, name = "naimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_munitsip")
    private String naimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_organa_munitsipalnogo_kontrolya_s_kotorym_pro;
    @Column(length = 2048, name = "informatsiya_o_postanovlenii_o_naznachenii_administrativnogo_na")
    private String informatsiya_o_postanovlenii_o_naznachenii_administrativnogo_naznacheniya_ili_reshenii_o_priostanovl;
    @Column(length = 2048, name = "informatsiya_o_prisvoenii_deyatelnosti_yuridicheskogo_litsa_yul")
    private String informatsiya_o_prisvoenii_deyatelnosti_yuridicheskogo_litsa_yul_i_individualnogo_predprinimatelya_ip;
    @Column(length = 2048, name = "nomer_proverki_v_sisteme_as_sppivp")
    private String nomer_proverki_v_sisteme_as_sppivp;
}
