package com.alesharik.backend.data;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface ScanTargetEntityRepo extends CrudRepository<ScanTargetEntity, Long> {
    @Query("SELECT entity FROM ScanTargetEntity entity WHERE entity.target = :target")
    Optional<ScanTargetEntity> findByTarget(@Param("target") String target);
}
