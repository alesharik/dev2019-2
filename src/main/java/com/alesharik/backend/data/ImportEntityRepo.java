package com.alesharik.backend.data;

import org.springframework.data.repository.CrudRepository;

public interface ImportEntityRepo extends CrudRepository<ImportEntity, Long> {
}
