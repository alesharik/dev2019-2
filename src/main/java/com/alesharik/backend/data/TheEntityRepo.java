package com.alesharik.backend.data;

import org.springframework.data.repository.CrudRepository;

public interface TheEntityRepo extends CrudRepository<TheEntity, Long> {
}
