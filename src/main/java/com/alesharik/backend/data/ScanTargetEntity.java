package com.alesharik.backend.data;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class ScanTargetEntity {
    @Id
    @GeneratedValue
    private long id;

    @Column(length = 2048)
    private String target;

    @OneToMany
    private List<TheEntity> owners;
}
