package com.alesharik.backend.data;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@javax.persistence.Entity
@Data
public class ImportEntity {
    @Id
    @GeneratedValue
    private long id;

    @Column(length = 2048)
    private String naimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_munitsipalnogo_kontrolya;
    @Column(length = 2048)
    private String naimenovanie_yuridicheskogo_litsa_filiala_predstavitelstva_obosoblennogo_strukturnogo_podrazdeleniya;
    @Column(length = 2048)
    private String adres_mesta_nakhozhdeniya_yul;
    @Column(length = 2048)
    private String adres_mesta_fakticheskogo_osushchestvleniya_deyatelnosti_yul_ip;
    @Column(length = 2048)
    private String adres_mesta_nakhozhdeniya_obektov;
    @Column(length = 2048)
    private String osnovnoy_gosudarstvennyy_registratsionnyy_nomer_ogrn;
    @Column(length = 2048)
    private String identifikatsionnyy_nomer_nalogoplatelshchika_inn;
    @Column(length = 2048)
    private String tsel_provedeniya_proverki;
    @Column(length = 2048)
    private String osnovanie_provedeniya_proverki_data_gosudarstvennoy_registratsii_yul_ip;
    @Column(length = 2048)
    private String osnovanie_provedeniya_proverki_data_okonchaniya_posledney_proverki;
    @Column(length = 2048)
    private String osnovanie_provedeniya_proverki_data_nachala_osushchestvleniya_yul_ip_deyatelnosti_v_sootvetstvii_s_p;
    @Column(length = 2048)
    private String osnovanie_provedeniya_proverki_inye_osnovaniya_v_sootvetstvii_s_federalnym_zakonom;
    @Column(length = 2048)
    private String data_nachala_provedeniya_proverki;
    @Column(length = 2048)
    private String srok_provedeniya_planovoy_proverki_rabochikh_dney;
    @Column(length = 2048)
    private String srok_provedeniya_planovoy_proverki_rabochikh_chasov_dlya_msp_i_mkp;
    @Column(length = 2048)
    private String forma_provedeniya_proverki_dokumentarnaya_vyezdnaya_dokumentarnaya_i_vyezdnaya;
    @Column(length = 2048)
    private String naimenovanie_organa_gosudarstvennogo_kontrolya_nadzora_organa_munitsipalnogo_kontrolya_s_kotorym_pro;
    @Column(length = 2048)
    private String informatsiya_o_postanovlenii_o_naznachenii_administrativnogo_naznacheniya_ili_reshenii_o_priostanovl;
    @Column(length = 2048)
    private String informatsiya_o_prisvoenii_deyatelnosti_yuridicheskogo_litsa_yul_i_individualnogo_predprinimatelya_ip;
    @Column(length = 2048)
    private String nomer_proverki_v_sisteme_as_sppivp;

}
