package com.alesharik.backend;

import com.sun.org.glassfish.external.amx.AMX;
import lombok.experimental.UtilityClass;

import java.util.HashMap;
import java.util.Map;

@UtilityClass
public class CryptoUtils {
    public static String encrypt(String text, final String key) {
        StringBuilder res = new StringBuilder();
        text = text.toUpperCase();
        for (int i = 0, j = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (c < 'А' || c > 'Я') continue;
            res.append((char) ((c + key.charAt(j) - 2 * 'А') % 26 + 'А'));
            j = ++j % key.length();
        }
        return res.toString();
    }

    public static String decrypt(String text, final String key) {
        StringBuilder res = new StringBuilder();
        text = text.toUpperCase();
        for (int i = 0, j = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (c < 'А' || c > 'Я') continue;
            res.append((char) ((c - key.charAt(j) + 26) % 26 + 'А'));
            j = ++j % key.length();
        }
        return res.toString();
    }
}
